from fastapi import status, HTTPException, Header
from typing import Optional
import jwt
from jwt.exceptions import InvalidTokenError
from constants import SECRET_KEY


async def check_token(bearer_token: Optional[str] = Header(None, alias="Authorization")):
    if not bearer_token or not bearer_token.startswith("Bearer "):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Access denied")
    _, _, token = bearer_token.partition("Bearer ")
    if not token:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Access denied")
    try:
        decoded = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
    except InvalidTokenError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Access denied")
    else:
        return decoded
