from fastapi import HTTPException, APIRouter, Depends, status
from pydantic import BaseModel
from utils import load_db
from auth import check_token

router = APIRouter()
fake_database = load_db()


class ChangePasswordBody(BaseModel):
    old_password: str
    new_password: str


class ChangePasswordResponse(BaseModel):
    username: str
    status: str = "done"


@router.put("/{username}", response_model=ChangePasswordResponse)
async def change_password(username: str, body: ChangePasswordBody, auth_payload: dict = Depends(check_token)):
    user_in_db = fake_database.get(username)
    if not user_in_db:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    request_user = auth_payload["username"]
    if request_user != username:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Access denied")
    if user_in_db["password"] != body.old_password:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Old password is incorrect")
    # logic change pass
    fake_database[username]["password"] = body.new_password
    return {"username": request_user}
