from fastapi import FastAPI
from endpoints import login
from endpoints import users

app = FastAPI()

app.include_router(login.router, prefix="/login")
app.include_router(users.router, prefix="/users")
